from prompt_toolkit.application import Application
from prompt_toolkit.layout.layout import Layout

import yaga.views
import yaga.bindings


if __name__ == '__main__':
    container = yaga.views.container_create(yaga.bindings.ux_manager)

    yaga_app = Application(
        layout=Layout(container),
        key_bindings=yaga.bindings.kb_app,
        mouse_support=True,
        full_screen=True,
        after_render=yaga.bindings.on_app_rendered,
        style=yaga.views.UI_THEME)

    yaga_app.run()
