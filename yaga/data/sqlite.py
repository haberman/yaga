import os
import sqlite3
import re


class Db:
    def __init__(self, database, queries_init=None):
        self.database = database
        self.queries_init = queries_init

    def _create(self):
        open(self.database, 'a').close()

        if self.queries_init is not None:
            self.conn = sqlite3.connect(self.database)
            self.cursor = self.conn.cursor()
            self.cursor.executescript(';\n'.join(self.queries_init))
            self.close()

    def connect(self):
        if not os.path.isfile(self.database):
            self._create()

        self.conn = sqlite3.connect(self.database, detect_types=sqlite3.PARSE_COLNAMES)
        self.cursor = self.conn.cursor()

    def close(self):
        self.conn.commit()
        self.conn.close()

    def count(self, table='authors'):
        self.connect()
        self.cursor.execute(f'SELECT COUNT(*) FROM {table}')

        result = self.cursor.fetchone()

        if result:
            result = result[0]

        self.close()
        return result

    def fetch_one(self, table, id):
        query = f'SELECT * FROM {table} WHERE {table[:-1]}_id = ?'
        self.connect()
        self.cursor.execute(query, (id,))

        res = self.cursor.fetchone()
        self.close()
        return res

    def fetch_multiple(self, table, ids, join=None):
        ph = '?,' * len(ids)
        query = f'SELECT * FROM {table}'
        if type(join) is tuple:
            query += f' INNER JOIN {join[0]} ON {join[1]} = {join[2]}'
        query += f' WHERE {table[:-1]}_id IN ({ph[:-1]})'

        self.connect()
        self.cursor.execute(query, tuple(ids))

        res = self.cursor.fetchall()
        self.close()
        return res

    def fetch_distinct(self, row, table='books'):
        '''Returns all distinct values for the row of a given table.'''

        self.connect()

        select = 'SELECT book_subjects' if row == 'subjects' else f'SELECT DISTINCT {table[:-1]}_{row}'
        query = f'{select} FROM `{table}`;'
        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()

        if row == 'subjects':
            return set([s.strip() for r in res for s in r[0].split('--')])  # unique list of subjects

        return res

    def fetch_all(self, table='books', sort_by=None, order=None, limit=None):
        ''' Returns all rows of given table. '''

        self.connect()

        query = f'SELECT * FROM {table}'
        if sort_by and order:
            query += f'ORDER BY {sort_by} {order}'
        elif sort_by:
            query += f'ORDER BY {sort_by}'
        if limit:
            query += f'{query} LIMIT {limit}'

        self.cursor.execute(query)

        res = self.cursor.fetchall()
        self.close()
        return res

    def fetch_form(self, form):
        '''Constructs the query of a given plain search form.'''

        query = 'SELECT * FROM books INNER JOIN authors ON author_id = book_authorid'
        tables_map = {'by': ('author_name', ' LIKE ', ' NOT LIKE ', '%{}%'),
                      'about': ('book_subjects', ' LIKE ', ' NOT LIKE ',  '%{}%'),
                      'in': ('book_language', '=', '!=', '{}')}

        no_filters = True
        for f in form['filters'].keys():
            if f != 'lim' and len(form['filters'][f]) > 0:
                no_filters = False
                break

        if not no_filters and form['query'] == '':
            query += ' WHERE ('
        elif form['query'] != '':
            query += f' WHERE book_title LIKE "%{form["query"]}%"'
            query += f' OR author_name LIKE "%{form["query"]}%"'

            if not no_filters:
                query += ' AND ('

        i = 0
        limit = ''

        for f in form['filters'].keys():
            filter_items = form['filters'][f]
            if filter_items is None:         # when lim is not set
                continue
            elif type(filter_items) is int:  # sets lim
                limit = f'LIMIT {filter_items}'
                continue
            elif len(filter_items) == 0:     # there's nothing to do with that filter
                continue

            if i > 0:
                query += ') AND ('
            for j in range(len(filter_items)):
                it = filter_items[j]
                table, op, not_op, val = tables_map[f]

                op_search = re.search(r'^(and|or|not)', it)
                if not op_search and j == 0:           # mostly meaning -> first operator isn't `not`
                    query += table + op + f'"{val.replace("{}", it)}"'
                    continue

                op_key = op_search.group(1)            # operator key
                op_val = it[op_search.end():].strip()  # query value after the operator

                if op_val[:3] == 'not':                # opertor is followed by `not`
                    query += table + not_op + f'"{val.replace("{}", op_val[3:].strip())}"'
                elif op_key == 'not':                  # operator is a a single `not`
                    if j > 0:                          # and it's not the first operator on the list
                        query += ' and '
                    query += table + not_op + f'"{val.replace("{}", op_val)}"'
                else:                                  # in any other case (and|or) -> appends the query
                    query += f' {op_key} ' + table + op + f'"{val.replace("{}", op_val)}"'

            i += 1

        if not no_filters:
            query += ')'

        self.connect()
        try:
            self.cursor.execute(query + f' {limit}')
            res = self.cursor.fetchall()
            self.close()

            return res
        except sqlite3.OperationalError as e:
            from yaga.bindings import ux_manager

            ux_manager.console_append('w', f'sqlite3.OperationalError: {e} for query: {query}')

    def update_one_stat(self, stat_id, stat_value):
        query = 'INSERT INTO stats VALUES (?,?) ON CONFLICT(stat_id) DO UPDATE SET stat_value=?'
        self.connect()
        self.cursor.execute(query, (stat_id, stat_value, stat_value))

        self.close()
        return True

    def add_one(self, table, values):
        ph = '?,' * len(values)
        query = f'INSERT INTO {table} VALUES ({ph[:-1]})'
        return self.exec(query, values)

    def add_many(self, table, batch):
        query = f'INSERT INTO {table} VALUES '

        flat_values = []
        for values in batch:
            ph = '?,' * len(values)
            query += f'({ph[:-1]}),'
            flat_values.extend(values)

        return self.exec(query[:-1], flat_values)

    def exec(self, query, values):
        self.connect()

        if type(values) is not tuple:
            values = tuple(values)

        try:
            with self.conn:
                self.conn.execute(query, values)
        except sqlite3.IntegrityError as e:
            return f'sqlite error: {e.args[0]}'

        except KeyError as e:
            return f'missing column error: {e.args[0]}'

        finally:
            self.close()

        return True

    def delete_all(self, table='books'):
        ''' Used for testing '''

        self.connect()
        self.cursor.execute(f'DELETE FROM {table}')
        self.close()
