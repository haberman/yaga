import os
import re
import tarfile
import urllib3
import asyncio

import xml.etree.ElementTree as ET

from . import sqlite

URL_GUTENBERG_BZ2 = 'https://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.bz2'
URL_GUTENBERG_FILES = 'https://www.gutenberg.org/files'

PATH_DATA_FOLDER = os.path.abspath('./data')
PATH_DB_FILE = f'{PATH_DATA_FOLDER}/yaga.db'
PATH_BZ2_FILE = f'{PATH_DATA_FOLDER}/rdf-files.tar.bz2'

QUERY_CREATE_STATS = '''CREATE TABLE IF NOT EXISTS stats
                        (stat_id VARCHAR PRIMARY KEY,
                         stat_value VARCHAR,
                         CONSTRAINT stat_unique UNIQUE(stat_id));
                        INSERT INTO `stats` VALUES ('rdfs_count', 0), ('books_count', 0), ('txts_count', 0)'''
QUERY_CREATE_AUTHORS = '''CREATE TABLE IF NOT EXISTS authors
                          (author_id VARCHAR PRIMARY KEY,
                           author_name VARCHAR,
                           author_birthdate INTEGER,
                           author_deathdate INTEGER,
                           author_webpage VARCHAR)'''
QUERY_CREATE_BOOKS = '''CREATE TABLE IF NOT EXISTS books
                        (book_id VARCHAR PRIMARY KEY,
                         book_title VARCHAR,
                         book_language VARCHAR,
                         book_subjects VARCHAR,
                         book_authorid VARCHAR,
                         FOREIGN KEY(book_authorid) REFERENCES authors(author_id))'''
QUERY_CREATE_URLS = '''CREATE TABLE IF NOT EXISTS urls
                       (url_id INTEGER PRIMARY KEY,
                        url_epub VARCHAR,
                        url_kindle VARCHAR,
                        url_html VARCHAR,
                        url_txt VARCHAR,
                        book_id VARCHAR,
                        FOREIGN KEY(book_id) REFERENCES books(book_id))'''

QUERY_BATCH_SIZE = 50


db = sqlite.Db(PATH_DB_FILE, [QUERY_CREATE_STATS, QUERY_CREATE_AUTHORS, QUERY_CREATE_BOOKS, QUERY_CREATE_URLS])

# Gutenberg RDF is behind https
urllib3.disable_warnings()
pool = urllib3.PoolManager()


async def queue_produce(queue, data, sleep=.00001):
    await queue.put(data)
    await asyncio.sleep(sleep)  # required or the UI isn't refreshed


def _validate_bz2():
    '''Checks that an existing bz2 archive is of the same size than the hosted one.'''

    if not os.path.isfile(PATH_BZ2_FILE):
        return False, 0

    req = catalog_request()
    remote_size = int(req.info().getheaders('Content-Length')[0])
    local_size = os.path.getsize(PATH_BZ2_FILE)
    req.release_conn()

    return bool(local_size == remote_size), remote_size


def _validate_db():
    '''Checks that an existing sqlite file contains the update amount of records, regarding the last `books_count` stats.'''

    books_count = db.fetch_one('stats', 'books_count')
    actual_count = db.count('books')

    if actual_count:
        return int(books_count[1]) == actual_count, actual_count
    else:
        return False, None


def _validate_rdfs():
    '''Gets the last uncompressed book from the `stats` info.'''

    rdfs_count = db.fetch_one('stats', 'rdfs_count')

    if rdfs_count and int(rdfs_count[1]) != 0:
        return int(rdfs_count[1])
    else:
        rdf_folder = f'{PATH_DATA_FOLDER}/rdf'
        if not os.path.isdir(rdf_folder):
            os.makedirs(rdf_folder)
        return False


def _validate_txts():
    txts_folder = f'{PATH_DATA_FOLDER}/txt'
    if not os.path.isdir(txts_folder):
        os.makedirs(txts_folder)
        return False
    else:
        return len(os.listdir(txts_folder))


async def data_validate(log_console=True):
    '''Async that chains the logic of data validation.

    Returns:
        A future for a tuple made of a list of statuses, inactive commands
        and a bool indicating whether the application should be invalidated or not.
    '''
    from yaga.bindings import KEYS_CMD

    statuses = {'db': {'valid': False, 'message': f'database {os.path.basename(PATH_DB_FILE)} is empty'},
                'rdfs': {'valid': False, 'message': 'decompressed catalog is unsynced'},
                'bz2': {'valid': None, 'message': None},
                'txt': {'valid': False, 'message': 'there is no text files to play with'}}

    cmd_inactives = []

    if os.path.isfile(PATH_DB_FILE):
        db_valid, books_count = _validate_db()

        if books_count and books_count > 0:
            statuses['db']['message'] = f'database is usable over {books_count} books'
        else:
            cmd_inactives.append(KEYS_CMD[2][1])

        if db_valid:
            statuses['db']['valid'] = True
            statuses['db']['message'] = f'fully synced database of {books_count} books'
    else:
        cmd_inactives.append(KEYS_CMD[2][1])

    rdfs_archive_count = _validate_rdfs()
    if rdfs_archive_count:
        rdfs_local_count = len(os.listdir(f'{PATH_DATA_FOLDER}/rdf'))
        if rdfs_local_count == rdfs_archive_count:
            statuses['rdfs']['valid'] = True
            statuses['rdfs']['message'] = f'unarchived catalog is valid for a total of {rdfs_archive_count} rdf files'
        else:
            percent = '{:.2%}'.format(rdfs_local_count/rdfs_archive_count)
            remains = rdfs_archive_count-rdfs_local_count
            statuses['rdfs']['valid'] = False
            statuses['rdfs']['message'] = f'{percent} of the catalog has been decompressed (missing {remains} files)'

    txts_count = _validate_txts()
    if txts_count:
        statuses['txt']['valid'] = True
        statuses['txt']['message'] = f'there is a total of {txts_count} text files to play with'
    else:
        cmd_inactives.append(KEYS_CMD[3][1])

    internet = misc_check_internet()
    if internet:
        bz2_valid, bz2_size = _validate_bz2()
        if bz2_valid:
            statuses['bz2']['valid'] = True
            statuses['bz2']['message'] = 'the local copy of the Gutenberg archive is up to date'
            statuses['bz2']['data'] = bz2_size
        else:
            statuses['bz2']['valid'] = False
            statuses['bz2']['message'] = 'the Gutenberg archive is outdated'
            statuses['bz2']['data'] = bz2_size
        return (statuses, cmd_inactives, False, log_console)
    else:
        return (statuses, cmd_inactives, True, log_console)


def catalog_request():
    '''Returns a pool request for the Gutenberg bz2 catalog.'''

    res = pool.request('GET', URL_GUTENBERG_BZ2, preload_content=False)
    return res


async def catalog_download(queue):
    '''Async that downloads the Gutenberg bz2 catalog.

    Args:
        queue (asuncio.Queue): A queue to inform consumer of download progress.
    '''
    req = catalog_request()
    file_sz = int(req.info().getheaders('Content-Length')[0])
    await queue_produce(queue, ('n'))

    with open(PATH_BZ2_FILE, 'wb') as bz2_file:
        dl_sz = 0
        block_sz = 8192 * 4

        i = 0
        while True:
            buffer = req.read(block_sz)
            if not buffer:
                break

            dl_sz += len(buffer)
            bz2_file.write(buffer)

            if i % 10 == 0 or dl_sz >= file_sz:
                await queue_produce(queue, ('p', dl_sz / file_sz, 'Downloading', '  '))

            i += 1

    req.release_conn()
    await queue_produce(queue, ('p', 1, 'Downloading', '  '))


async def catalog_decompress(queue):
    '''Async that decompress the Gutenberg bz2 archive.

    Args:
        queue (asuncio.Queue): A queue to inform consumer of decompression progress.
    '''
    rdf_folder = f'{PATH_DATA_FOLDER}/rdf'
    rdf_listdir = os.listdir(rdf_folder)

    decompressed_count = 0
    with tarfile.open(PATH_BZ2_FILE, 'r:bz2') as tar:
        members = tar.getmembers()
        members_count = len(members)

        await queue_produce(queue, ('t', f'Syncing {members_count} rdfs...'))
        await queue_produce(queue, ('n'))

        for i in range(members_count):
            member = members[i]
            if 'DELETE' in member.name or '999999' in member.name:
                continue

            rdf_file = tar.extractfile(member)
            member_basename = os.path.basename(member.name)
            try:
                rdf_listdir.index(member_basename)
            except ValueError:
                with open(f'{rdf_folder}/{member_basename}', 'wb+') as rdf:
                    rdf.write(rdf_file.read())
                if i % 10 == 0 or i == members_count - 1:  # for 100% display ensurance
                    await queue_produce(queue, ('p', i / members_count, 'Decompressing', '  '))

            decompressed_count += 1

        db.update_one_stat('rdfs_count', decompressed_count)
        await queue_produce(queue, ('p', 1, 'Decompressing', '  '))


async def catalog_insert(queue):
    '''Async that insert the unarchivaed rdfs records into the sqlite database.

    Args:
        queue (asuncio.Queue): A queue to inform consumer of insertion progress.
    '''
    NS = {'pgterms': 'http://www.gutenberg.org/2009/pgterms/',
          'dcterms': 'http://purl.org/dc/terms/',
          'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
          'marcrel': 'http://id.loc.gov/vocabulary/relators/'}

    rdf_folder = f'{PATH_DATA_FOLDER}/rdf'
    rdf_listdir = os.listdir(rdf_folder)
    rdf_count = len(rdf_listdir)
    await queue_produce(queue, ('n'))

    books_batch = []
    authors_batch = []
    urls_batch = []

    for i in range(rdf_count):
        rdf_file = rdf_listdir[i]
        rdf_path = f'{rdf_folder}/{rdf_file}'
        rdf_book = ET.parse(rdf_path).find('.//pgterms:ebook', NS)
        rdf_id = rdf_book.attrib[rdf_book.keys()[0]]

        # we don't want no author
        author = rdf_book.find('.//dcterms:creator//pgterms:agent', NS)
        if not author:
            author = rdf_book.find('.//marcrel:ctb//pgterms:agent', NS)
        if not author:
            continue

        # we don't want unknown authors
        author_values = [author.attrib[author.keys()[0]]]
        if author_values[0] == '2009/agents/49':
            continue

        # we don't want books with none european alphabets
        language = rdf_book.find('.//dcterms:language', NS)
        if language:
            language = language.find('.//rdf:value', NS).text

            if language == 'zh' or language == 'ja' or language == 'ko':
                continue

        # we don't want no subject
        subject = rdf_book.findall('.//dcterms:subject', NS)
        if len(subject) == 0:
            continue

        book_values = [rdf_id]
        book_int_id = re.match(r'^ebooks/(\d+)', book_values[0]).group(1)
        url_values = [book_int_id, None, None, None, None]
        for f in rdf_book.findall('.//pgterms:file', NS):
            res_url = f.attrib[f.keys()[0]]
            res_type = f.find('.//rdf:Description/rdf:value', NS).text.strip()
            res_ext = '.'.join(res_url.split('.')[-2:])

            if res_ext == 'epub.images':
                url_values[1] = res_url
            elif res_ext == 'kindle.images':
                url_values[2] = res_url
            elif res_ext == 'html.images':
                url_values[3] = res_url
            elif res_ext == 'txt.utf-8' or (res_url[-4:] == '.txt' and res_type == 'text/plain; charset=utf-8'):
                url_values[4] = res_url

        # we don't want audio books
        if url_values[4] is None:
            continue

        author_db = db.fetch_one('authors', author_values[0])
        if author_db is None:
            for key in ['name', 'birthdate', 'deathdate', 'webpage']:
                value = author.find(f'.//pgterms:{key}', NS)
                if value is None:
                    author_values.append(None)
                elif key == 'webpage':
                    author_values.append(value.attrib[value.keys()[0]])
                else:
                    author_values.append(value.text)

            authors_batch.append(author_values)

        book_db = db.fetch_one('books', book_values[0])
        if book_db is None:
            title = rdf_book.find('.//dcterms:title', NS).text.splitlines()[0]  # fetch only the first line of the title
            book_values.append(title)
            book_values.append(language)

            subjects = []
            for s in subject:
                t_subject = s.find('.//rdf:value', NS).text
                if len(t_subject) <= 3:
                    continue
                elif '--' in t_subject:
                    for ss in t_subject.split('--'):
                        ss = ss.strip()
                        if not ss in subjects:
                            subjects.append(ss)
                else:
                    subjects.append(t_subject)

            book_values.append(' -- '.join(subjects))
            book_values.append(author_values[0])  # author foreign key
            books_batch.append(book_values)

            # actually insert the resources' urls
            url_values.append(book_values[0])     # book foreign key
            urls_batch.append(url_values)

        update_progress = False
        if len(books_batch) >= QUERY_BATCH_SIZE:
            db.add_many('books', books_batch)
            books_batch = []
            db.add_many('urls', urls_batch)
            urls_batch = []

            update_progress = True

        if len(authors_batch) >= QUERY_BATCH_SIZE:
            db.add_many('authors', authors_batch)
            authors_batch = []

            update_progress = True

        if update_progress:
            await queue_produce(queue, ('p', i / rdf_count, 'Inserting', '  '))

    db.update_one_stat('books_count', db.count('books'))
    await queue_produce(queue, ('p', 1, 'Inserting', '  '))


async def txts_download(ids, queue):
    ''' Downloads texts to the local folder. '''

    await queue_produce(queue, ('n'))

    ids_count = len(ids)
    ids_reject = []
    for i in range(ids_count):
        book_int_id = ids[i]
        book_path = f'{PATH_DATA_FOLDER}/txt/pg{book_int_id}.txt'
        if os.path.exists(book_path):
            continue

        books_urls = db.fetch_one('urls', book_int_id)
        res = pool.request('GET', books_urls[4])
        with open(book_path, 'w') as txt_file:
            try:
                txt_file.write(res.data.decode('utf-8'))
            except UnicodeDecodeError:
                ids_reject.append(book_int_id)
                continue

        if i % 2 == 0 or i == ids_count - 1:
            await queue_produce(queue, ('p', i / ids_count, 'Fetching', '  '))

    if len(ids_reject) > 0:
        await queue_produce(queue, ('w', f'Couldn\'t decode files for ebook ids: {", ".join(ids_reject)}.'))
    else:
        await queue_produce(queue, ('p', 1, 'Fetching', '  '))

    await queue_produce(queue, ('e'))


def misc_check_internet():
    # return False  # debug faster
    internet = True
    try:
        pool.request('GET', 'http://google.com')
    except (urllib3.exceptions.NewConnectionError, urllib3.exceptions.MaxRetryError):
        internet = False

    return internet
