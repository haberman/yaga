import os
import re
import asyncio

from prompt_toolkit.key_binding import KeyBindings

from . import ux


KEYS_APP = {'c-q': 'Quit',
            'c-h': 'Help',
            's-left': 'Focus sidebar',
            's-right': 'Focus main window',
            's-c': 'Toggle console visibility'}
KEYS_CMD = [('c-p', 'pull', 'Pull remote data'),
            ('c-c', 'clear', 'Clear local data'),
            ('c-f', 'find', 'Query catalog'),
            ('c-e', 'explore', 'Explore files')]

ux_manager = ux.UXManager()
sleep_time = .00001


def browser_on_download():
    '''Callback function for when the user requests to download books.'''
    from yaga.data import txts_download

    ids = []
    for l in ux_manager.browser[1].content.buffer.document.lines:
        id_ = re.match(r'(\d+)', l.split('|')[1].strip())
        if id_:
            ids.append(id_.group(1))

    ux_manager.console_append('t', f'Downloading {len(ids)} books…')

    queue = asyncio.Queue()
    asyncio.gather(txts_download(ids, queue), on_queue(queue))


def browser_on_poetrify():
    '''Callback function for when the user wants to extract / analyze poetry.
       -> open the poetrify dialog.'''
    ux_manager.app.layout.focus(ux_manager.dialog_poetrify)


def dialog_on_poetrify_submit(form):
    '''Callback function for when the poetrify dialog is submitted
       -> fills the browser with results from database crossing an existing local file.'''
    from yaga.data import db,  PATH_DATA_FOLDER
    from yaga.nlp import poetrify_books

    if 'poetry' not in form['filters']['about']:
        if len(form['filters']['about']) > 0:
            form['filters']['about'].append('and poetry')
        else:
            form['filters']['about'] = ['poetry']

        # this is the only nlp covered language for now
        form['filters']['in'] = ['en']

    db_books = db.fetch_form(form)
    db_ids = [re.match(r'ebooks/(\d+)', b[0]).group(1) for b in db_books]
    txt_ids = [re.match(r'pg(\d+).txt', f).group(1)
               for f in os.listdir(f'{PATH_DATA_FOLDER}/txt/') if f.endswith('.txt')]

    ids = [i for i in db_ids if i in txt_ids]
    books = [b for b in db_books if re.match(r'ebooks/(\d+)', b[0]).group(1) in ids]

    queue = asyncio.Queue()
    asyncio.gather(poetrify_books(queue, books), on_queue(queue))

    ux_manager.pane_focus = 1
    ux_manager.browser_set_books(books, 'poetrify')


def dialog_on_find_submit(form):
    '''Callback function for when the query of the `find` dialog is submitted
       -> fills the browser with results from database.'''
    from yaga.data import db
    res = db.fetch_form(form)

    ux_manager.pane_focus = 1
    ux_manager.browser_set_books(res, 'find')


async def cmd_pull(queue):
    '''Async that chains logic of the `pull` command.

    Args:
        queue (asyncio.Queue): A queue instance to inform a consumer about tasks' overall progression.
    '''
    from yaga.data import (data_validate, PATH_BZ2_FILE, PATH_DB_FILE, URL_GUTENBERG_BZ2,
                           catalog_decompress, catalog_download, catalog_request, catalog_insert)
    validation_data = await data_validate()
    statuses = validation_data[0]
    no_action = True

    if not statuses['bz2']['valid']:
        await queue.put(('t', f'Saving Gutenberg catalog from {URL_GUTENBERG_BZ2}…'))
        await asyncio.sleep(sleep_time)
        await catalog_download(queue)

        statuses['rdfs']['valid'] = False  # since we've grabbed a new copy, re-sync rdfs decompression
        no_action = False

    if not statuses['rdfs']['valid']:
        await queue.put(('t', f'Opening archive {os.path.basename(PATH_BZ2_FILE)}, this may take a while…'))
        await asyncio.sleep(sleep_time)
        await catalog_decompress(queue)

        statuses['db']['valid'] = False  # since we may have uncompressed new rdf, re-sync database inserts
        no_action = False

    if not statuses['db']['valid']:
        await queue.put(('t', f'Syncing database {os.path.basename(PATH_DB_FILE)} with rdfs catalog…'))
        await asyncio.sleep(sleep_time)
        await catalog_insert(queue)

        no_action = False

    await asyncio.sleep(sleep_time)

    if no_action:
        await queue.put(('i', '[pull]> No action have been taken'))
        await asyncio.sleep(sleep_time)
    else:
        await queue.put(('s', 'Done!'))
        await asyncio.sleep(sleep_time)

    # refresh commands availability
    if not no_action:
        task = asyncio.get_event_loop().create_task(data_validate(False))
        task.add_done_callback(on_data_validation)


def cmd_explore():
    from yaga.data import PATH_DATA_FOLDER, db

    book_ids = []
    for txt_file in os.listdir(f'{PATH_DATA_FOLDER}/txt/'):
        if not txt_file.endswith('.txt'):
            continue
        book_id = re.match(r'pg(\d+).txt', txt_file).group(1)
        book_ids.append(f'ebooks/{book_id}')

    res = db.fetch_multiple('books', book_ids, ('authors', 'author_id', 'book_authorid'))
    ux_manager.browser_set_books(res, 'explore')

    return True


def cmd_clear():
    from yaga.data import PATH_DATA_FOLDER

    # TODO @incomplete -> yet used to clean all json files from /txts folder
    i = 0
    for f in os.listdir(f'{PATH_DATA_FOLDER}/json'):
        if f.endswith('.json') or f.endswith('.ndjson'):
            os.remove(f'{PATH_DATA_FOLDER}/json/{f}')
            i += 1

    ux_manager.console_append('i', f'Cleared a total of {i} files...')


def cmd_on_click():
    '''Callback for when the user clicks a command.
       -> calls the corresponding method and tracks its eventual async progress using asyncio.'''

    assert ux_manager.status['data']

    active_cmd = ux_manager.cmd_get_focused()[0]

    queue = asyncio.Queue()
    producer = None

    if active_cmd[1] == 'pull':
        producer = cmd_pull(queue)
    elif active_cmd[1] == 'clear':
        cmd_clear()
    elif active_cmd[1] == 'find':
        ux_manager.app.layout.focus(ux_manager.dialog_find)
    elif active_cmd[1] == 'explore':
        cmd_explore()

    if producer:
        asyncio.gather(producer, on_queue(queue))


async def on_queue(queue):
    '''Async that consumes a command task progression queue.'''

    while True:
        item = await queue.get()
        operator = item[0]

        before = ' '
        before += item[3] if len(item) == 4 else ''

        if operator == 'e':
            queue.task_done()
            ux_manager.console_line_clear()
            try:
                ux_manager.console_append('i', item[2])
            except IndexError:
                ux_manager.console_append('s', 'Done!')
        elif operator == 'n':
            ux_manager.console_line_new()
        elif operator != 'p':
            ux_manager.console_append(item[0], item[1], before)
        else:              # percentage
            ux_manager.console_progress(item[1], f'{item[2]} - ', before)


def on_data_validation(future):
    '''Callbacks that receives the `future` of the opening `validate_data` call.'''

    assert ux_manager.app

    data_statuses, cmd_inactives, invalidate_app, log_console = future.result()

    if log_console:
        for k in data_statuses.keys():
            status = data_statuses[k]
            if not status['message']:
                continue

            if status['valid']:
                ux_manager.console_append('s', status['message'], '   ')
            else:
                ux_manager.console_append('f', status['message'], '   ')
        ux_manager.console_line_new()

    for cmd in [k[1] for k in KEYS_CMD]:
        try:
            cmd_inactives.index(cmd)
            ux_manager.cmd_set_active(cmd, False)
        except ValueError:
            ux_manager.cmd_set_active(cmd, True)

    if invalidate_app:  # @weird but required if the callback isn't set from a future executor
        ux_manager.app.invalidate()

    ux_manager.status['data'] = True


def on_dialog_cancel():
    '''Callback function for when the dialog is canceled -> set focus back to left pane.'''

    ux_manager.pane_focus = 0


kb_app = KeyBindings()
kb_cmd = KeyBindings()
kb_exp = KeyBindings()


def on_app_rendered(app):
    '''Callback called only once, after the prompt_toolkit app is rendered.

    Args:
        app (Application): The running prompt_toolkit application instance.
    '''
    if ux_manager.app:
        return

    from yaga.data import PATH_DATA_FOLDER, data_validate

    if not os.path.exists(PATH_DATA_FOLDER):
        os.makedirs(PATH_DATA_FOLDER)

    ux_manager.init(app)
    task = asyncio.get_event_loop().create_task(data_validate())
    task.add_done_callback(on_data_validation)

    @kb_app.add('c-q', eager=True)
    def _(event):
        event.app.exit()

    @kb_app.add('s-left')
    def _(event):
        ux_manager.pane_focus = 0

    @kb_app.add('s-right')
    def _(event):
        ux_manager.pane_focus = 1

    @kb_app.add('s-down')
    def _(event):
        ux_manager.status['console'] = False

    @kb_app.add('s-up')
    def _(event):
        ux_manager.status['console'] = True

    @kb_cmd.add('up')
    def _(event):
        cmd_focused = ux_manager.cmd_get_focused()
        cmd_actives = ux_manager.cmd_get_actives()
        cmd_next = cmd_focused[1] - 1

        if cmd_next >= 0 and cmd_actives.index(cmd_next) >= 0:
            event.app.layout.focus_previous()
        elif cmd_next == -1:  # loop
            ux_manager.cmd_set_focus(KEYS_CMD[cmd_actives[-1]][1])

    @kb_cmd.add('down')
    def _(event):
        cmd_focused = ux_manager.cmd_get_focused()
        cmd_actives = ux_manager.cmd_get_actives()
        cmd_next = cmd_focused[1] + 1

        if cmd_next < len(cmd_actives) and cmd_actives.index(cmd_next) >= 0:
            event.app.layout.focus_next()
        elif cmd_next == len(cmd_actives):  # loop
            ux_manager.cmd_set_focus(KEYS_CMD[cmd_actives[0]][1])

    @kb_exp.add('tab')
    def _(event):
        ux_manager.app.layout.focus_next()

    @kb_exp.add('s-tab')
    def _(event):
        ux_manager.app.layout.focus_previous()
