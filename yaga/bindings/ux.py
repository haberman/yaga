import re

from prompt_toolkit.application import Application
from prompt_toolkit.buffer import Buffer, Document
from prompt_toolkit.layout.controls import FormattedTextControl

import yaga.data
import yaga.views
import yaga.bindings


class UXManager(object):
    '''Entry point to interact with the Applicaton UI.'''

    def __init__(self):
        self.app = None
        self.panes = None
        self.browser = None
        self.commands = None
        self.dialog_find = None
        self.dialog_poetrify = None
        self.console_buffer = None

        self._pane_focus = None
        self._status = {'data': False, 'console': True}

    def init(self, app):
        '''The public initiation method that sets up the manager.

        Args:
            app (prompt_toolkit.application.Application): The running `Application` instance.
        '''
        self.app = app
        self.panes = self.app.layout.container.get_children()[0].get_children()[1].get_children()
        self.browser = self.panes[1].get_children()[0].content.get_children()
        self.commands = self.panes[0].get_children()[1].get_children()
        self.dialog_find = self.app.layout.container.floats[0].content.get_children()[0]
        self.dialog_poetrify = self.app.layout.container.floats[1].content.get_children()[0]
        self.console_buffer = self.panes[1].get_children()[1].content.get_children()[1].content.buffer

    def _cmd_to_index(self, cmd_name):
        '''Returns the index for the given `cmd_name` button.'''

        cmds = [k[1] for k in yaga.bindings.KEYS_CMD]
        return cmds.index(cmd_name)

    def cmd_get_focused(self):
        '''Returns a `(cmd, index)` tuple for the currently focused command button.'''

        assert isinstance(self.app, Application)
        cmd_focus = yaga.bindings.KEYS_CMD[0]

        for i in range(len(self.commands)):
            command_btn = self.commands[i]
            if self.app.layout.has_focus(command_btn):
                cmd_focus = yaga.bindings.KEYS_CMD[i]
                break

            cmd_focus = yaga.bindings.KEYS_CMD[i]

        return (cmd_focus, i)

    def cmd_set_focus(self, cmd):
        '''Set the focus to the matching `cmd` button.'''

        cmd_idx = self._cmd_to_index(cmd)
        self.app.layout.focus(self.commands[cmd_idx])

    def cmd_get_actives(self):
        '''Returns indices of the active command buttons.'''

        cmd_actives = []
        for i in range(len(self.commands)):
            if self.commands[i].style == '':
                cmd_actives.append(i)

        return cmd_actives

    def cmd_set_active(self, cmd, active=True):
        '''
        Toggles the `class:inactive` style of command buttons.

        Args:
            cmd (tuple): Member of the `yaga.data.KEY_CMD` array;
            active (bool): Whether toe focused button should be active or not.
        '''
        cmd_idx = self._cmd_to_index(cmd)
        cmd_btn = self.commands[cmd_idx]

        if active:
            cmd_btn.style = ''
        else:
            cmd_btn.style = 'class:inactive'

    def browser_set_books(self, books, mode='find'):
        '''Fills up books and gives focus to the browser.'''
        from yaga.nlp.parser import remove_accents
        from yaga.views import UI_EXPLORE_BROWSER_MAP, UI_FIND_BROWSER_MAP

        ui_map = UI_FIND_BROWSER_MAP
        if mode == 'explore':
            ui_map = UI_EXPLORE_BROWSER_MAP

        from yaga.views.layouts import browser_header
        fmtxt_header = browser_header(ui_map)
        self.browser[0].content = FormattedTextControl(fmtxt_header)

        from yaga.views.layouts import browser_buttons
        self.browser[3] = browser_buttons(mode)

        if len(books) == 0:
            self.console_append('w', f'[{mode}]> No results were found.')
        else:
            self.console_append('i', f'[{mode}]> Found {len(books)} books.')

        book_lines = []
        for book in books:
            line = ' '
            if mode == 'explore':
                line = ''

            for i in range(len(ui_map)):
                col = ui_map[i]
                content = book[col[0]].strip()

                if len(col) > 4:  # we have a regex to test against content
                    content = re.match(col[4], content).group(1)

                if len(col) > 5:  # a callable will fill our content
                    content = col[5](content)

                elif len(col) > 3 and type(col[3]) is int:
                    content_lenth = len(remove_accents(content))
                    if content_lenth > col[3]:
                        content = content[:col[3]]
                    elif col[2] == 'left':
                        content = content.ljust(col[3])
                    elif col[2] == 'right':
                        content = content.rjust(col[3])

                if i < len(ui_map) - 1:
                    if col[1] == ' ':  # glues the file status right to the pipe character
                        content += '| '
                    else:              # add one blank padding for better readability
                        content += ' | '

                line += content

            book_lines.append(line)

        book_buffer = self.browser[1].content.buffer
        book_buffer.set_document(Document('\n'.join(book_lines), 0), True)
        self.app.layout.focus(book_buffer)

    def _console_scroll_down(self):
        '''Scrolls down the console pannel to its last line.'''
        self.console_buffer.cursor_position = len(self.console_buffer.document.text)

    # TODO @incomplete is there a way to know the width limit of the log area
    # and fills that line with a custom char?
    def console_line_new(self, char=None):
        '''Appends a line to the log TextArea that is either filled with the given `char` or plain blank.'''

        assert isinstance(self.console_buffer, Buffer)

        before = '\n'
        if char:
            txt = f'fill with {char}'

        txt = self.console_buffer.document.text
        txt += before

        self.console_buffer.set_document(Document(txt, 0))
        self._console_scroll_down()

    def console_line_clear(self):
        '''Clears the last line of the buffer.'''
        assert isinstance(self.console_buffer, Buffer)

        doc_lines = self.console_buffer.document.lines[:-1]

        self.console_buffer.set_document(Document('\n'.join(doc_lines), 0))
        self._console_scroll_down()

    def console_append(self, level, content, before=' '):
        '''Appends a new line to the log TextArea.

        Args:
            level (str): A letter (either: `i`, `s`, `f` `t`, or `w`) to be converted into a unicode dingbat;
            content (str): Content text to log;
            before (str): An eventual text to append before the logs (default: ` `).
        '''

        assert isinstance(self.console_buffer, Buffer)

        title = before
        if level == 'i':    # info
            title += u'\U0001F6C8'
        elif level == 's':  # success
            title += u'\U00002713'
        elif level == 'f':  # failure
            title += u'\U0001F5D9'
        elif level == 't':  # task
            title += u'\U000021D2'
        elif level == 'w':  # warning
            title += u'\U000026A0'

        txt = self.console_buffer.document.text
        txt += f'\n{title} {content}'

        self.console_buffer.set_document(Document(txt, 0))
        self._console_scroll_down()

    def console_join(self, content, before=' '):

        assert isinstance(self.console_buffer, Buffer)

        txt = self.console_buffer.document.text
        txt += before + content

        self.console_buffer.set_document(Document(txt, 0))
        self._console_scroll_down()

    def console_inplace(self, content, before=' '):
        '''Replace the last line of the logs' buffer with the given content.

        Args:
            content (str): Content text to log;
            before (str): An eventual text to append before the content (default: ' ').
        '''
        assert isinstance(self.console_buffer, Buffer)

        content = before + content
        input_lines = content.splitlines()
        doc_lines = self.console_buffer.document.lines[:-len(input_lines)]

        self.console_buffer.set_document(Document('\n'.join(doc_lines+input_lines), 0))
        self._console_scroll_down()

    def console_progress(self, progress, title='Task progress: ', before=' '):
        '''Shortcut method to log a percentage.

        Args:
            progress (float): Amount of progress (between 0 & 1);
            title (str): String to append before formatting the percentage.
        '''
        self.console_inplace('{} {:.2%}'.format(title, progress), before)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        self._status = value

    @property
    def pane_focus(self):
        '''Returns the vertical focused index, either 0 (the sidebar) or 1 (the body).'''

        assert isinstance(self.app, Application)

        self._pane_focus = 0
        if self.app.layout.has_focus(self.panes[0]):
            self._pane_focus = 0
        elif self.app.layout.has_focus(self.panes[1]):
            self._pane_focus = 1

        return self._pane_focus

    @pane_focus.setter
    def pane_focus(self, value):
        '''Sets the focus for the vertical split by changing the corresponding `self.panes` style.'''

        assert isinstance(self.app, Application)

        self._pane_focus = value
        self.app.layout.focus(self.panes[self._pane_focus])

        if self._pane_focus:
            self.panes[1].style = 'class:pane.focused'
            self.panes[0].style = 'class:pane'
        else:
            self.panes[0].style = 'class:pane.focused'
            self.panes[1].style = 'class:pane'
