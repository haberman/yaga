import os
import re
import json

from statistics import median, variance
from itertools import groupby
from operator import itemgetter

from yaga.data import queue_produce

from . import parser

CHUNK_RANGE_SZ = 50
CHUNK_LINE_SZ = 60

MERGE_RANGE = 4
MERGE_LINE = 2

SEEK_MAX = 30
VERSE_MAX = 66


def line_is_header_end(line):
    return ('start of project gutenberg' in line or
            'start of the project gutenberg' in line or
            'start of this project gutenberg' in line or
            '*end*the small print!' in line or
            '["small print"' in line)  # -> shakespeare is special


def line_is_footer_start(line):
    return ('end of project gutenberg' in line or
            'end of the project gutenberg' in line or
            'end of this project gutenberg' in line or
            'end of this etext' in line or
            'here ends the project gutenberg' in line)


def line_clean(s):
    '''
    Removes leading and trailing numbers with whitespace.
    '''
    match = re.search(r'( {3,}\d+\.?)$', s)
    if match:
        s = s[:match.start()]
    s = re.sub(r'\[\d+\]', '', s)
    return s


def lines_slice(lines_idx, ranges, range_idx, snap_under):
    '''
    Function that will temper a list of `range` to spread new lines under a given index.

    Args:
        lines_idx (list): an array of lines's indices to to get text;
        ranges (list): the lines' lookup indices;
        range_idx (int): where to mutate `ranges`;
        snap_under (int): range seperated by less that that value will be merged.

    '''
    del ranges[range_idx]

    ranges_contig = ranges_contiguous(sorted(lines_idx), snap_under)
    for r in ranges_contig:
        if len(r) > 0:
            ranges.insert(range_idx, r)


def lines_strip(lines, ranges, range_idx):
    '''
    Removes empty lines at the beginning or the end of a range.

    Args:
        lines (list): an array of lines to get text;
        range_input (range): the lines' lookup indices;
        range_idx (int): where to mutate `ranges`;

    Returns:
        A new clamped range
    '''
    start, stop = ranges[range_idx].start, ranges[range_idx].stop

    # if lines[start] == '':
    #     start += 1
    while start < stop and lines[start] == '':
        start += 1
    while stop > 0 and lines[stop] == '':
        stop -= 1

    ranges[range_idx] = range(start, stop)


def lines_collect_footnote(lines, start, stop):
    '''
    Returns a `set` of lines' indices for which we've collected footnotes.

    Args:
        lines (list): text to inspect as a list of lines;
        start  (int): start index of the lookup;
        stop   (int): stop index of the lookup.

    Returns:
        notes (set): a unique list of indices holding lines of note.
    '''
    re_enter = re.compile(r'^\s*(\[Footnote)|(FOOTNOTE)')
    re_bracket_exit = re.compile(r'\][\S]{0,1}$')

    # first line of that range doesn't start a stanza
    if start > 0 and lines[start-1] != '':
        return set()

    fn_gaps = set()
    fn_entered = False

    for i in range(start, stop+1):          # if we can't find an exit by the last line, one more may give us a blank line
        line = lines[i]

        if not fn_entered and line == '':   # consume blank lines as long as we're not in a footnote
            continue

        if not fn_entered and re_enter.match(line):
            fn_entered = True

        if fn_entered:
            fn_gaps.add(i)

        if re_bracket_exit.search(line) or line == '':
            fn_entered = False
            continue

    return fn_gaps


def lines_collect_edition(lines, start, stop):
    '''
    Returns a `set` of lines' indices for which we've found an edition block.

    @see lines_find_note
    '''
    line = lines[start]
    editions = set()

    if not re.search(r'(CONTENTS|PREFACE|NOTES|INTRODUCTION)', line):
        return set()

    in_edition = False
    editions.add(start)
    exit_found = False
    i = stop
    for i in range(start+1, stop+1):
        line = lines[i]
        editions.add(i)

        if not in_edition and line == '':
            continue
        else:
            in_edition = True

        # we exit on double blank lines
        if line == '' and len(lines) > i-1 and lines[i+1] == '':
            exit_found = True
            break

    if exit_found:
        return editions
    elif i == stop:
        return set()

    print('we should never reach that point')
    return editions


def lines_filter_blobs(lines, range_input, max_seek=SEEK_MAX):
    '''
    Filters each line of an input `range` from blobs of text being either notes or editions.
    If a block is caught in the middle of a range, it will cause a slice that we may want to glue back.
    For that, we temper `lines` at the opening index with a `>>gap{start,stop}<<` tag.

    Args:
        lines (list): lines of text to inspect
        range_input (range): the `range` to filter;
        parsers (list): a list of function to test each line;
        max_seek (int): maximum amount of lines we'll fetch while searching for a block exit.

    Returns:
        A list of integers containing the indices of filtered lines
    '''
    start, stop = range_input.start, range_input.stop
    lines_keep = set([l for l in range(start, stop+1)])

    eds = lines_collect_edition(lines, start, stop+1)
    lines_keep -= eds
    if len(lines_keep) == 0:
        return set()

    fns = lines_collect_footnote(lines, start, stop)
    lines_keep -= fns
    if len(lines_keep) == 0:
        return set()

    gaps = ranges_contiguous(sorted(fns), 1)

    for gap in gaps:
        lines[gap.start] = f'>>gap[{gap.start},{gap.stop}]<<'
        lines_keep.add(gap.start)

    return sorted(lines_keep)


def range_is_poem(lines, start, stop, line_size=VERSE_MAX, check_span=8):
    if start == stop:
        return False

    if start >= stop:
        stop_ = stop
        stop = start
        start = stop_

    j = -1
    poem_score = 0
    para_score = 0

    while j < 0 and j > -check_span:
        if stop+j < start:        # the beginning of the range
            break
        try:
            rw_line = lines[stop+j]
            if rw_line == '':     # continue if line is empty
                j -= 1
                continue

            if len(rw_line) < line_size * .8:
                poem_score += 1
            else:
                para_score += 1

            line_checks = {k: f('', rw_line) for k, f in parser.poetry_checks_tight.items()}
            if all(line_checks):  # tempers para score if that line passes poetry tight checks
                para_score -= .5

        except IndexError:        # the beginning of the entire document
            break

        j -= 1

    return poem_score > para_score


def ranges_contiguous(ints, snap_under):
    '''
    Transforms a list of continuous integers into a list of contiguous ranges.
    '''
    ranges = []
    for idx in groupby(enumerate(ints), lambda x: x[0]-x[1]):
        group = map(itemgetter(1), idx[1])
        group = list(map(int, group))
        start, stop = group[0], group[-1]
        if len(ranges) > 0:
            prev_start, prev_stop = ranges[-1].start, ranges[-1].stop
            if start - prev_stop <= snap_under:
                ranges[-1] = range(prev_start, stop)
                continue

        ranges.append(range(start, stop))

    return ranges


def ranges_sanitize_lines(ranges, lines, snap_under):
    '''
    Inspects input ranges lines (stanzas) and sanitize them..

    Args:
        ranges (list): the list of lines' ranges to inspect;
        lines (list): the text to sanitize;
        snap_under (int): a value under which stanza should contiguous stanzas should be snapped.
    Returns:
        A sanitized list of ranges.
    '''

    # ignore very small

    ranges = sorted(ranges, key=lambda x: x.start)
    ranges_ = [(r, i) for i, r in enumerate(ranges)]  # if len(r) >= 2

    if len(ranges_) == 0:
        return ranges

    ranges_sanitized = ranges.copy()
    for range_ in sorted(ranges_, key=lambda x: x[1], reverse=True):
        range_val = range_[0]
        range_idx = range_[1]

        lines_keep = lines_filter_blobs(lines, range_val)
        if len(lines_keep) == 0:                     # range can be removed
            del ranges_sanitized[range_idx]
            continue
        elif len(lines_keep) - 1 == len(range_val):  # range is to left untouched
            lines_strip(lines, ranges_sanitized, range_idx)
            continue

        lines_slice(lines_keep, ranges_sanitized, range_idx, snap_under)
        lines_strip(lines, ranges_sanitized, range_idx)

    return ranges_sanitized


def ranges_spread_chunks(ranges, lines, snap_under, range_size=CHUNK_RANGE_SZ, line_size=CHUNK_LINE_SZ):
    '''
    Inspects longs ranges and attempts a tighter poetry extract on them.
    Parameters configure what shape a chunk should be given.

    Args:
        ranges (list): a list of `range` delimiting the lines to inspect;
        lines (list): the text itself as a list of lines;
        range_size (int): minimum size of a range to be considered a chunk;
        line_size (int): average lines' size of a chunk should be above that amount.
        A copy of the input ranges with updated chunks.
    '''
    ranges = sorted(ranges, key=lambda x: x.start)
    chunks = [(r, i) for i, r in enumerate(ranges) if len(r) >= range_size]

    if len(chunks) == 0:
        return ranges

    ranges_spread = ranges.copy()

    for chunk in sorted(chunks, key=lambda x: x[1], reverse=True):
        start, stop = chunk[0].start, chunk[0].stop
        range_idx = chunk[1]

        lines_size = [len(l.strip()) for l in lines[start:stop]]
        line_med_size = median(lines_size)

        # not chunky enough
        if line_med_size < line_size:
            continue

        i = start - 1
        line_prev = ''
        lines_keep = []

        while i < stop:
            i += 1
            line = lines[i]
            # only consider lines being 20% shorter than median line size
            if len(line) > line_med_size * .8:
                line_prev = line
                continue

            # next line is either blank or the last one on the list
            checks = parser.poetry_checks_tight.items()
            if i == stop - 1 or(i < stop - 1 and lines[i+1] == ''):
                if range_is_poem(lines, start, stop, line_med_size * .8):
                    checks = parser.poetry_checks_loose.items()
                else:
                    line_prev = line
                    continue

            line_checks = {k: f(line_prev.lower(), line) for k, f in checks}
            if all(line_checks.values()):
                lines_keep.append(i)

            line_prev = line

        lines_slice(lines_keep, ranges_spread, range_idx, snap_under)

    return ranges_spread


def stanzas_relax(ranges, lines, gap_merge):
    '''
    Collects ranges' fragments and aggregates those that we can.
    Also rejects oneliners and ranges covering single stanzas that appears not to be poems.
    '''
    def to_stanzas(ranges, reverse=False):
        return [{'range': r, 'gaps': []} for r in sorted(ranges, key=lambda x: x.start, reverse=reverse)]

    def stanza_strip(stzs, stz_idx):
        start, stop = stzs[stz_idx]['range'].start, stzs[stz_idx]['range'].stop

        while start < stop and lines[start] == '':
            start += 1
        while stop > 0 and lines[stop] == '':
            stop -= 1

        stzs[stz_idx]['range'] = range(start, stop)

    re_gap = re.compile(r'>>gap\[(\d+),(\d+)\]<<')

    ranges_ = [(r, i) for i, r in enumerate(ranges)]
    ranges_ = sorted(ranges_, key=lambda x: x[1])

    stanzas = to_stanzas(ranges)
    stanza_idx = len(stanzas)
    while stanza_idx > 0:
        stanza_idx -= 1
        range_ = stanzas[stanza_idx]['range']
        range_next = stanzas[stanza_idx+1]['range'] if stanza_idx < len(stanzas)-1 else None
        snappable = range_next and range_next.start - range_.stop <= gap_merge
        oneliner = len(range_) == 0

        # deletes floating oneliners
        if oneliner and not snappable:
            line = lines[range_.start]
            line_checks = {k: f('', line) for k, f in parser.poetry_checks_tight.items()}
            if len(line) > VERSE_MAX or not all(line_checks.values()):
                del stanzas[stanza_idx]
                continue
        else:
            # deletes stanzas that art not poems
            if not range_is_poem(lines, range_.start, range_.stop):
                del stanzas[stanza_idx]
                continue

            # collect gaps
            i = range_.start
            while i < range_.stop + 1:
                line = lines[i]
                line_gap = re_gap.search(line)
                if line_gap:
                    gap_start, gap_stop = int(line_gap.group(1)), int(line_gap.group(2))
                    stanzas[stanza_idx]['gaps'].append(range(gap_start, gap_stop))

                    # range_next = stanzas[stanza_idx+1]['range'] if stanza_idx < len(stanzas)-1 else None
                    # the gap is within the range
                    # this stanza can ne snapped with its following one
                    if range_next and snappable and stanza_idx < len(stanzas)-1:
                        stanzas[stanza_idx]['gaps'].extend(stanzas[stanza_idx+1]['gaps'])
                        stanzas[stanza_idx]['range'] = range(range_.start, range_next.stop)
                        del stanzas[stanza_idx+1]
                        i = gap_stop
                    else:                         # if not, withdraw line that was marked as a gap
                        stanzas[stanza_idx]['range'] = range(range_.start, range_.stop - 1)

                i += 1

        stanza_strip(stanzas, stanza_idx)

    return stanzas


def stanzas_release(stanzas, lines):
    crumbs = [s for s in stanzas if len(s['range']) <= 3]
    print(f'we have {len(crumbs)} crumbs')
    print(crumbs)

    return stanzas_to_poems(stanzas, lines)


def stanzas_to_poems(stanzas, lines, with_gaps=True):
    '''
    Aggregates text for ranges and returns a `JSON` like list of poems.
    '''
    poems = []
    stanzas = sorted(stanzas, key=lambda x: x['range'].start)
    stanzas_count = len(stanzas)
    for i in range(0, stanzas_count):
        start, stop = stanzas[i]['range'].start, stanzas[i]['range'].stop
        poem = {'range': (start, stop), 'verses': [lines[j] for j in range(start, stop+1)]}

        if with_gaps and len(stanzas[i]['gaps']) > 0:
            gaps = sorted(stanzas[i]['gaps'], key=lambda x: x.start)
            poem['gaps'] = [(h.start, h.stop) for h in gaps]

        poems.append(poem)

    return poems


def stanzas_to_lines(stanzas, lines):
    '''
    Flattens stanzas into a list of lines excluding gaps.
    '''
    poem_lines = []
    stanzas = sorted(stanzas, key=lambda x: x['range'].start)
    stanzas_count = len(stanzas)
    for i in range(0, stanzas_count):
        start, stop = stanzas[i]['range'].start, stanzas[i]['range'].stop
        gaps = sorted(stanzas[i]['gaps'], key=lambda x: x.start)

        if len(gaps) == 0:
            poem_lines.extend([{'gid': j, 'str': lines[j]} for j in range(start, stop+1)])
        else:
            for j in range(start, stop+1):
                if all([j not in h for h in gaps]):
                    poem_lines.append({'gid': j, 'str': lines[j]})

    return poem_lines


# async def poetrify_file(queue, path, merge_lines=MERGE_LINE, merge_stanzas=MERGE_RANGE):
async def poetrify_file(queue, path, merge_lines=MERGE_LINE, merge_stanzas=MERGE_RANGE):
    lines = []
    verses = []
    lines_count = 0
    gutenberg_lines_count = 0
    eof = False

    # first pass: versify -> dimiss Gutenberg notice and loosely checks if remaining lines are poetry
    with open(path, 'r') as txt_file:
        lines = txt_file.read().splitlines()
        lines_count = len(lines)

        # that's a dummy notice for multi volume book that we can't fetch
        if lines_count <= 40:
            return [], [], 0, 0

        i = -1
        line_prev = ''
        # while i < lines_count:
        while i < lines_count:
            i += 1  # we really want to increment early since we're exiting the loop at multiple times

            try:
                line = line_clean(lines[i])
            except IndexError:
                break  # eof

            line_lower = line.lower()

            # current line is the beginning of a Gutenberg footer
            # -> there's nothing more for us to do...
            if line_is_footer_start(line_lower):
                gutenberg_lines_count += lines_count-i
                eof = line_lower
                break

            # more that 80% of the line looks like roman numerals
            l = line.replace(' ', '')
            if sum([1 for c in l if re.search(r'[IVXDC]', c)]) > len(l)*.8:
                continue

            # current line is the end of a Gutenberg header
            # -> clears verses since actual content hasn't begun
            if line_is_header_end(line_lower):
                gutenberg_lines_count += i
                verses = []
                line_prev = line
                continue

            # line opens an edition block parsed further away
            editions = lines_collect_edition(lines, i, len(lines))
            if len(editions) > 0:
                verses.append((line, i))
                verses.extend([(lines[idx], idx) for idx in editions])
                i = max(editions)
                continue

            # same thing with line that opens a note
            if re.search(r'[_\[]', line.strip()):
                verses.append((line, i))
                continue

            # tests line against each loose regex
            line_checks = {k: f(line_prev.lower(), line) for k, f in parser.poetry_checks_loose.items()}
            if all(line_checks.values()):
                verses.append((line, i))

            line_prev = line

    pass_counts = []

    # getting poetry with 5 passes
    ranges = ranges_contiguous([v[1] for v in verses], merge_lines)
    pass_counts.append(len(ranges))

    lines_raw = lines.copy()

    ranges = ranges_spread_chunks(ranges, lines, merge_lines)
    pass_counts.append(len(ranges))

    ranges = ranges_sanitize_lines(ranges, lines, merge_lines)
    pass_counts.append(len(ranges))

    stanzas = stanzas_relax(ranges, lines, merge_stanzas)
    pass_counts.append(len(stanzas))

    # poems = stanzas_release(stanzas, lines)
    # pass_counts.append(len(poems))

    poems = stanzas_to_poems(stanzas, lines_raw)
    # poems = stanzas_to_poems([{'range': r} for r in ranges], lines, False)

    if not eof:
        if queue:
            await queue_produce(queue, ('w', f'couldn\'t find an eof for file {path}'))
        # else:
        # print(f'Couldn\'t find an eof for file {path}')
        return [], [], 0, 0

    # return poems, [], pass_counts, 0
    # print(lines_count - gutenberg_lines_count)
    ndjson_lines = stanzas_to_lines(stanzas, lines_raw)
    # print(len(ndjson_lines))
    return poems, ndjson_lines, pass_counts, len(ndjson_lines) / (lines_count - gutenberg_lines_count)


async def poetrify_books(queue, books, save_lines=True, export_types=['lines', 'poems']):
    '''Poetrify a list of books coming from a sqlite query.'''

    from yaga.data import PATH_DATA_FOLDER
    from yaga.bindings import ux_manager

    books_count = len(books)
    # books_count = 50
    await queue_produce(queue, ('n'))

    dismiss_count = 0
    for i in range(books_count):
        book = books[i]
        book_int_id = re.match(r'^ebooks/(\d+)', book[0]).group(1)
        txt_file = f'{PATH_DATA_FOLDER}/txt/pg{book_int_id}.txt'
        json_file = f'{PATH_DATA_FOLDER}/json/pg{book_int_id}-poems.json'

        if os.path.exists(json_file):
            continue

        poems, lines, pass_counts, poetry_ratio = await poetrify_file(queue, txt_file)

        if poems is None or poetry_ratio < .25:
            dismiss_count += 1
            continue

        if not save_lines:
            await queue_produce(queue, ('i', f'Caught {len(lines)} of poetry, stanzas statistics by pass: {pass_counts}'))
            continue

        await queue_produce(queue, ('p', float(i/books_count), f'Parsing {str(book_int_id).ljust(5)}', '  '))
        for t in export_types:
            if t == 'poems':
                with open(json_file, 'w') as poems_export:
                    json.dump(poems, poems_export)
            if t == 'lines':
                with open(f'{PATH_DATA_FOLDER}/json/pg{book_int_id}-lines.ndjson', 'w') as lines_export:
                    lines_export.write('\n'.join([json.dumps(line) for line in lines]))

    it = tuple('e')
    if dismiss_count > 0:
        it = ('e', f'{dismiss_count} books have been dismissed for having a low ratio of poetry')

    await queue_produce(queue, it)
    return True
