import re
import unicodedata

blacklist = []

# this is basically what a 167 github stared python module looks like
# @see https://github.com/dariusk/wordfilter


# def blacklist_init(datafile):
#     global blacklist
#     with open(datafile, 'r') as f:
#         blacklist = f.read().splitlines()


# def blacklisted(string):
#     string = string.lower()
#     return any([word in string for word in blacklist])


# def blacklist_add(words):
#     assert type(words) is list
#     blacklist.extend([w.lower() for w in words])


# def blacklist_remove(words):
#     assert type(words) is list

#     for w in words:
#         try:
#             blacklist.remove(w.lower())
#         except ValueError:
#             pass


# blacklist_init(f'{PATH_DATA_FOLDER}/wordfilter.txt')


def remove_accents(string):
    """
    Removes unicode accents from a string, downgrading to the base character
    """

    nfkd = unicodedata.normalize('NFKD', string)
    return u"".join([c for c in nfkd if not unicodedata.combining(c)])

# @see https://github.com/aparrish/gutenberg-poetry-corpus

# costly and very small chance to appear (caught in header or footer)
# re_http = r'((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*'
# re_mail = r'^([0-9a-zA-Z](?>[-.\w]*[0-9a-zA-Z])*@(?>[0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$'


poetry_checks_loose = {
    # not below 6 characters (inclusive)
    'length': lambda prev, curr: len(curr) >= 6,
    # isn't a blank line following a blank line
    'not_double_blank': lambda prev, curr: not (prev == '' and curr == ''),
    # if the last line was long and this one is short, it's probably the end of a paragraph
    'not_last_para_curr': lambda prev, curr: not (len(prev) >= 70 and len(curr) <= 60),
    # isn't in title case
    'not_title_case': lambda prev, curr: not curr.istitle(),
    # doesn't begin or end with a digit
    'not_number': lambda prev, curr: not re.search(r'^\d', curr) and not re.search(r'\d$', curr),
    # passes the wordfilter
    # 'not_blacklisted': lambda prev, curr: not blacklisted(curr),

    # descending to chars...
    # not more than 50% upper-case characters
    'not_mostly_upper': lambda prev, curr: (len([ch for ch in curr if ch.isupper()]) / (len(curr)+0.01)) < 0.5,
    # less than 25% of the line is punctuation characters
    'punct': lambda prev, curr: (len([ch for ch in curr if ch.isalpha() or ch.isspace()]) / (len(curr)+0.01)) > 0.75,
}

poetry_checks_tight = {

    # not below 6 characters (inclusive)
    # 'length': lambda prev, curr: len(curr) <= 68,
    # not all upper-case
    'not_upper': lambda prev, curr: not curr.isupper(),
    # for poetry in plays, it appears to be a norm that didaskalia are enclosed within underscores
    'no_didaskalia': lambda prev, curr: not re.search(r'^\s*_(.{5,})_[\S]{0,1}$', curr), #@dev we removed escaped \_ 
    # isn't title case when considering only longer words
    # 'not_mostly_title_case': lambda prev, curr: not ' '.join([w for w in curr.split() if len(w) >= 5]).istitle(),

    # descending to chars...
    # doesn't begin with a bracket (angle or square)
    'no_bracket': lambda prev, curr: not any([curr.startswith(ch) for ch in '[<'])
}
