# -*- coding: utf_8 -*-

PROG_NAME = 'YAGA'
PROG_VERSION = 0.7
PROG_VERSION_MINOR = 'b'
PROG_SNIPPET = 'Press [Ctrl-q] to quit, [Ctrl-h] for help'
PROG_DESCRIPTION = 'Yet Another Gutenberg Aggregator'
