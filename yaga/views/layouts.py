from pygments.token import Keyword, Name, Operator, Literal, Text

from prompt_toolkit.filters import Condition
from prompt_toolkit.layout.dimension import LayoutDimension as D
from prompt_toolkit.layout.containers import (Window, HSplit, VSplit,
                                              HorizontalAlign, VerticalAlign, WindowAlign,
                                              ConditionalContainer)
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.processors import Processor, Transformation
from prompt_toolkit.widgets import SearchToolbar, TextArea, Box, Button

from yaga import PROG_NAME, PROG_VERSION, PROG_VERSION_MINOR, PROG_SNIPPET


class HighlightCursorLineProcessor(Processor):
    """
    Processor that highlights the line under the cursor.
    """

    def apply_transformation(self, transformation_input):
        cursor_row = transformation_input.document.cursor_position_row
        line_no = transformation_input.lineno
        fragments = transformation_input.fragments
        if cursor_row == line_no:
            new_fragments = []
            for cls_style, content in fragments:
                new_fragments.append(
                    (f'{cls_style} class:line.focused', content))

            return Transformation(new_fragments)

        return Transformation(fragments)


def browser_buttons(mode, style='class:toolbar.btnbox'):
    btns = []
    if mode == 'find':
        from yaga.bindings import browser_on_download
        btn_download = Button('Download', width=12,
                              handler=browser_on_download)
        btns.append(Box(btn_download, padding=1, padding_top=0,
                    padding_bottom=0, style=f'{style}'))
    elif mode == 'explore':
        from yaga.bindings import browser_on_poetrify
        btn_poetrify = Button('Poetrify', width=12,
                              handler=browser_on_poetrify)
        btns.append(Box(btn_poetrify, padding=1, padding_top=0,
                    padding_bottom=0, style=f'{style}'))

    return VSplit(btns, style='class:theme.background', align=HorizontalAlign.RIGHT)


def browser_header(ui_map):
    fmtxt_header = []
    for i in range(len(ui_map)):
        col = ui_map[i]
        content = col[1]
        if len(col) > 3 and type(col[3]) is int:
            if col[2] == 'left':
                content = content.ljust(col[3])
            elif col[2] == 'right':
                content = content.rjust(col[3])
        elif len(col) == 2 and i == len(ui_map) - 1:
            content = content.ljust(400)  # header brute filling for last row

        fmtxt_header.append(('', f'{content} '))
        if i < len(ui_map) - 1:
            fmtxt_header.append(('class:col', '| '))

    return fmtxt_header


def _browser(mode, ui_map, key_bindings):
    fmtxt_header = browser_header(ui_map)
    action_buttons = browser_buttons(mode)

    # search_field = SearchToolbar(text_if_not_searching=[('class:not-searching', "Use / to start searching; Shift+Enter to download books' list.")])

    text_area = TextArea(read_only=True, wrap_lines=False, scrollbar=True,
                         input_processors=[HighlightCursorLineProcessor()])
    text_area.buffer.name = f'browser_{mode}'

    browser = HSplit([Window(FormattedTextControl(fmtxt_header),
                             height=D.exact(1), style='class:spreadsheet'),
                      text_area,
                      Window(char='_', height=D.exact(1)),
                      action_buttons,
                      Window(height=D.exact(1), style='class:theme.background')],
                     height=D(weight=2), key_bindings=key_bindings, modal=True)

    return ConditionalContainer(browser, Condition(lambda: text_area.buffer.document.line_count > 1))


# TODO @unused
def _document(title, content):
    '''Returns an `HSplit` consisting of a searchable plain text document.

    Args:
        title: The title of the document;
        content: The plain text content to display.
    '''
    search_field = SearchToolbar(text_if_not_searching=[(
        'class:not-searching', "Press '/' to start searching.")])
    text_area = TextArea(text=content, read_only=True,
                         scrollbar=True, search_field=search_field)
    text_area.buffer.name = 'document'

    doc = text_area.document
    statusbar = [('class:statusbar.title', title),
                 ('', ' - '),
                 ('class:statusbar.position', '{}:{}'.format(doc.cursor_position_row + 1,
                                                             doc.cursor_position_col + 1)),
                 ('', ' - Press '),
                 ('class:statusbar.keyword', '/'),
                 ('', ' for searching.')]

    return HSplit([Window(content=FormattedTextControl(statusbar),
                          height=D.exact(1),
                          style='class:statusbar'),
                   text_area,
                   search_field], height=D(weight=2))


def _console():
    '''Returns an `Hsplit` with a buffer set up to receive logs messages.'''

    from yaga.bindings import ux_manager

    text_area = TextArea()
    text_area.buffer.name = 'console'
    text_area.text = f'Hello poet!\n'

    console = HSplit([Window(content=FormattedTextControl([('class:titlebar.title', 'CONSOLE')]),
                             height=D.exact(1),
                             style='class:titlebar'),
                      text_area], height=D(weight=1))

    return ConditionalContainer(console, Condition(lambda: ux_manager.status['console']))


def _commands():
    '''Wraps the commands' list of buttons inside an `HSplit`.'''

    from yaga.bindings import KEYS_CMD, cmd_on_click, kb_cmd

    btns = []
    for cmd_data in KEYS_CMD:
        btn = Button(f' {cmd_data[1]} ', width=14, handler=cmd_on_click)
        btns.append(Box(btn, padding=0, padding_right=3,
                    padding_left=3, style='class:inactive'))

    return HSplit(btns, key_bindings=kb_cmd)


def header():
    '''Returns the main window header.'''

    title = FormattedTextControl([('bold', f'\n {PROG_NAME} {PROG_VERSION}{PROG_VERSION_MINOR}'),
                                  ('', ' | '),
                                  ('italic', f'{PROG_SNIPPET}')], focusable=False)

    return Window(content=title, height=D.exact(3), style='class:theme.background,theme.white', align=WindowAlign.CENTER)


def sidebar():
    return HSplit([Window(height=2), _commands()],
                  style='class:pane.focused', align=VerticalAlign.TOP)


def content():
    from yaga.views import UI_FIND_BROWSER_MAP
    from yaga.bindings import kb_exp

    # we start with a browser set to receive query results
    browser = _browser('find', UI_FIND_BROWSER_MAP, kb_exp)
    console = _console()
    # pager_container = _pager(title, content)

    return HSplit([browser, console], style='class:pane')
