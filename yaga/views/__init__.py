import os

from prompt_toolkit.styles import Style
from prompt_toolkit.layout.dimension import Dimension as D
from prompt_toolkit.layout.containers import FloatContainer, HSplit, VSplit, HorizontalAlign, VerticalAlign

from . import layouts
from . import dialogs
from yaga.data import PATH_DATA_FOLDER


UI_FIND_BROWSER_MAP = [(0, ' ', 'right', 1, r'^ebooks/(\d+)', lambda x: 'X' if os.path.exists(f'{PATH_DATA_FOLDER}/txt/pg{x}.txt') else ' '),
                       (0, 'id', 'right', 7, r'^ebooks/(\d+)'),
                       (6, 'author', 'left', 24),
                       (1, 'title', 'left', 36),
                       (2, 'lang', 'left', 4),
                       (3, 'subjects')]

UI_EXPLORE_BROWSER_MAP = UI_FIND_BROWSER_MAP[1:]  # in explore mode, we naturally dismiss the file status column

UI_THEME = Style.from_dict({'theme.background':                   'bg:#3c3c3c',
                            'theme.white':                        '#fff',
                            'pane':                               'bg:#1e1e1e',
                            'pane.focused':                       'bg:#252525',
                            'titlebar':                           'reverse',
                            'titlebar.title':                     'bold',
                            'toolbar.btnbox':                     'bg:#252525',
                            'toolbar.content':                    'bg:#252525',
                            'toolbar.keyword':                    'italic #000 bg:#ffaa00',
                            'button inactive':                    'bg:#1e1e1e #aaa',
                            'button.focused':                     'bold bg:#58585f #eccf98',
                            'text-area line.focused':             'reverse',
                            'spreadsheet':                        'bold bg:#eded7c #1e1e1e',
                            'spreadsheet.col':                    '#000',
                            'pygments.text':                      'underline #fff',  # query
                            'pygments.name':                      'italic #eded7c',  # and|or|not
                            'pygments.keyword':                   'bold #66e59d',    # by|about|in
                            'pygments.operator':                  'bold #e55c6c',    # lim keyword
                            'pygments.literal':                   'italic #fff',     # lim value
                            'dialog':                             'bg:#fff',
                            'dialog.body':                        'bg:#454545',
                            'dialog frame':                       'bg:#222d32 #222d32',
                            'dialog frame.label':                 'bg:#000 #fff',
                            'dialog button':                      'bold #fff',
                            'dialog button.focused':              'bg:#222d32 #fff',
                            'dialog radio-list':                  'fg:#fff',
                            'scrollbar.background':               'bg:#000',
                            'scrollbar.button':                   'bg:#008888',
                            'completion-menu.completion':         'bg:#008888 #fff',
                            'completion-menu.completion.current': 'bg:#00aaaa #000'
                            })


def container_create(ux_manager):
    sidebar_container = layouts.sidebar()
    main_container = layouts.content()
    find_dialog = dialogs.find()
    poetrify_dialog = dialogs.poetrify()

    body = VSplit([sidebar_container, main_container],
                  align=HorizontalAlign.JUSTIFY, padding=D.exact(1), padding_style='class:theme.background')

    return FloatContainer(
        content=HSplit([layouts.header(), body], align=VerticalAlign.JUSTIFY),
        floats=[find_dialog, poetrify_dialog])
