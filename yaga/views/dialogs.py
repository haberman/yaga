import re

from prompt_toolkit.application import get_app
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.completion import Completer, Completion
from prompt_toolkit.filters import has_focus
from prompt_toolkit.layout import Float
from prompt_toolkit.layout.containers import (
    HSplit, VSplit, Window, FloatContainer, ConditionalContainer)
from prompt_toolkit.layout.controls import BufferControl, FormattedTextControl
from prompt_toolkit.layout.dimension import D
from prompt_toolkit.layout.menus import CompletionsMenu
from prompt_toolkit.lexers import PygmentsLexer
from prompt_toolkit.widgets import Button, Dialog, Label

from pygments.token import Keyword, Name, Operator, Literal, Text
from pygments.lexer import RegexLexer, bygroups

import yaga.bindings
import yaga.data

INSTRUCTIONS_QUERY = [('fg:#fff', 'Filter with: '),
                      ('class:pygments.keyword', 'by'),
                      ('class:pygments.literal', ' authors'),
                      ('fg:#fff', ', '),
                      ('class:pygments.keyword', 'about'),
                      ('class:pygments.literal', ' subjects'),
                      ('fg:#fff', ', '),
                      ('class:pygments.keyword', 'in'),
                      ('class:pygments.literal', '\nlanguages'),
                      ('fg:#fff', ' and limit with: '),
                      ('class:pygments.operator', 'lim'),
                      ('class:pygments.literal', ' amount'), ('fg:#fff', '.')]

INSTRUCTIONS_POETRIFY = [('fg:#fff', 'Filter with: '),
                         ('class:pygments.keyword', 'by'),
                         ('class:pygments.literal', ' authors'),
                         ('fg:#fff', ' and'),
                         ('class:pygments.keyword', ' about'),
                         ('class:pygments.literal', ' subjects.'),
                         ('fg:#fff', '\nLimit with: '),
                         ('class:pygments.operator', 'lim'),
                         ('class:pygments.literal', ' amount'), ('fg:#fff', '.')]


class FindLexer(RegexLexer):
    '''Derived RegexLexer used to color the `find` input string.'''

    keywords = ['by', 'about', 'in', 'lim']
    keywords_pattern = r'\b(' + r'|'.join(keywords) + r')\b'
    filters_pattern = r'\b(' + r'|'.join(keywords[:-1]) + r')\b'

    operators = ['and', 'or', 'not']
    operators_pattern = r'\b(' + r'|'.join(operators) + r')\b'

    flags = re.IGNORECASE
    tokens = {
        'root': [
            (filters_pattern, Keyword),                       # filters
            (operators_pattern, Name),                        # operators
            (r'(lim\s+)(\d+)', bygroups(Operator, Literal)),  # lim sepcial case
            (r'^(.+?)(?=\s+'+keywords_pattern+r')', Text)     # query
        ]
    }


class PoetrifyLexer(RegexLexer):
    '''Derived QueryLexer that just exclude the `about` keyword.'''

    keywords = ['by', 'about', 'lim']
    keywords_pattern = r'\b(' + r'|'.join(keywords) + r')\b'
    filters_pattern = r'\b(' + r'|'.join(keywords[:-1]) + r')\b'

    operators = ['and', 'or', 'not']
    operators_pattern = r'\b(' + r'|'.join(operators) + r')\b'

    flags = re.IGNORECASE
    tokens = {
        'root': [
            (filters_pattern, Keyword),                       # filters
            (operators_pattern, Name),                        # operators
            (r'(lim\s+)(\d+)', bygroups(Operator, Literal)),  # lim sepcial case
            (r'^(.+?)(?=\s+'+keywords_pattern+r')', Text)     # query
        ]
    }


class QueryCompleter(Completer):
    '''Derived Completer that fills the right list of completions given the query context.'''

    completions_data = {'in': None, 'by': None, 'about': None}
    completions_pattern = r'\b(' + r'|'.join(completions_data) + r')\b'
    completions_active = ''

    def get_completions(self, document, complete_event):
        doc_txt = document.text.strip()
        if not len(doc_txt):
            return

        doc_words = doc_txt.split()
        if len(doc_words) < 2:
            return

        def word_matches(word):
            word = word.lower()
            return word.startswith(word_before_cursor)

        comp_matches = re.findall(QueryCompleter.completions_pattern, doc_txt)
        if len(comp_matches) == 0:
            return

        comp_key = comp_matches[-1]
        comp_active = QueryCompleter.completions_active
        comp_data = QueryCompleter.completions_data

        if comp_active != comp_key:
            # fills completion data only if `comp_key` hasn't bee set before.
            if comp_key == 'by' and not comp_data['by']:  # authors
                comp_data['by'] = [r[1] for r in yaga.data.db.fetch_all()]
            elif comp_key == 'about' and not comp_data['about']:
                comp_data['about'] = yaga.data.db.fetch_distinct('subjects')
            elif comp_key == 'in' and not comp_data['in']:
                comp_data['in'] = [r[0]
                                   for r in yaga.data.db.fetch_distinct('language')]

            comp_active = comp_key
            word_before_cursor = document.get_word_before_cursor(
                WORD=False).lower()

            for w in comp_data[comp_active]:
                if word_matches(w):
                    yield Completion(w, -len(word_before_cursor))


def _dialog(title, instructions, lexer, on_submit):
    def _query_accept(buf):
        '''Moves the cursor on the `OK` button if user press `enter` inside the query field.'''

        get_app().layout.focus_next()
        return True  # Keep text.

    def _subparse(string, matches, merge_adjacent=False):
        '''Parse a `string` and returns a list of (key, value) tuples for the input list of `Match`.

        Args:
            string (str): The input text to parse;
            matches (list): An array of `Match` for looking up keywords;
            merge_adjacent (bool): whether to merge matches when they're adjacents (eg: `or|and` + `not`).
        '''
        r = []
        k_merge = ''

        for i in range(len(matches)):
            m = matches[i]
            # values starts where keyword match ends
            v = string[m.end():]
            if i < len(matches)-1:                        # if it's not the last keyword…
                # then value goes up to the next match start index
                v = string[m.end():matches[i+1].start()]

            k = k_merge + m.group(1)
            v = v.strip()

            # keyword is to be merged whith the next one
            if merge_adjacent and len(v) == 0:
                k_merge = f'{k} '
                continue

            k_merge = ''
            r.append((k, v))

        return r

    def on_dialog_ok():
        '''Parses the plain input search into a formatted _query_ + _filers_ `dict`.'''

        txt = query_buffer.text

        form = {'query': txt, 'filters': {k: [] for k in keywords}}
        form['filters']['lim'] = None

        keywords_pattern = re.compile(lexer.keywords_pattern)
        operators_pattern = re.compile(lexer.operators_pattern)

        if re.match(r'^'+lexer.keywords_pattern, txt):    # first word of the query is a keyword
            form['query'] = ''
        else:                                                  # there's a query to find before any filter
            query_pattern = re.compile(
                r'^(.+?)(?=\s+'+lexer.keywords_pattern+r')')
            query_match = [m for m in query_pattern.finditer(txt)]
            if len(query_match) > 0:
                form['query'] = query_match[0].group(0).strip()

        keywords_match = [m for m in keywords_pattern.finditer(txt)]
        if len(keywords_match) > 0:
            for k, v in _subparse(txt, keywords_match):
                # removes uselessness
                v = re.sub(r'^(and|or)\s{1}', '', v)

                if k == 'lim':                                # lim single matches ints only
                    try:
                        form['filters']['lim'] = int(v)
                    except ValueError:
                        continue
                else:                                         # parse operators
                    operators_match = [
                        m for m in operators_pattern.finditer(v)]
                    if len(operators_match) > 0:
                        form['filters'][k] = [
                            f'{ok} {ov}' for ok, ov in _subparse(v, operators_match, True)]
                        # the first operator starts after a value
                        if operators_match[0].start() > 0:
                            form['filters'][k].insert(
                                0, v[:operators_match[0].start()].strip())
                    elif len(form['filters'][k]) > 0:
                        # no filters There's already an item
                        form['filters'][k].append(f'or {v}')
                    else:
                        form['filters'][k].append(v)

        on_submit(form)

    keywords = lexer.keywords[:-1]

    query_buffer = Buffer(completer=QueryCompleter(), complete_while_typing=True,
                          accept_handler=_query_accept, multiline=False)
    query_window = Window(BufferControl(buffer=query_buffer, lexer=PygmentsLexer(lexer)),
                          style='fg:#fff')
    query_input = FloatContainer(content=query_window,
                                 floats=[Float(CompletionsMenu(max_height=5, scroll_offset=1),
                                               xcursor=True, ycursor=True)])
    query_label = VSplit([Label(text='Query', width=8, style='bold fg:#fff'), query_input],
                         height=D(min=5))

    btn_ok = Button(text='OK', handler=on_dialog_ok)
    btn_cancel = Button(text='Cancel', handler=yaga.bindings.on_dialog_cancel)

    return Dialog(title=title, modal=True,
                  buttons=[btn_ok, btn_cancel],
                  body=HSplit([VSplit([Window(width=8),
                                       Window(FormattedTextControl(instructions),
                                              height=2, wrap_lines=True)]),
                               Window(height=1),
                               query_label],
                              padding=D(preferred=2)))


def find():
    '''Return a `Dialog` for querying the catalog.'''

    from yaga.bindings import KEYS_CMD, dialog_on_find_submit

    dialog = _dialog(KEYS_CMD[2][2],
                     INSTRUCTIONS_QUERY, FindLexer, dialog_on_find_submit)

    return Float(content=ConditionalContainer(content=dialog, filter=has_focus(dialog)),
                 width=60, height=12)


def poetrify():
    '''Return a `Dialog` for extracting / analyzing poetry'''

    from yaga.bindings import dialog_on_poetrify_submit

    dialog = _dialog('Extract / analyze poetry (en only)',
                     INSTRUCTIONS_POETRIFY, PoetrifyLexer, dialog_on_poetrify_submit)

    return Float(content=ConditionalContainer(content=dialog, filter=has_focus(dialog)),
                 width=60, height=12)
