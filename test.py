#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import json
from yaga.nlp import poetrify_file
from yaga.nlp.parser import poetry_checks_loose, poetry_checks_tight

# OK
# ebook_id = 'pg8799'
ebook_id = 'pg8905'
ebook_id = 'pg1105'

# low poetry
# ebook_id = 'pg9886'

# recursion errors
# ebook_id = 'pg20185'
# ebook_id = 'pg34117'
ebook_id = 'pg353'

TXTS_FOLDER = './data/txt'
JSON_FOLDER = './data/json'
# first_lines = []
# for f in os.listdir(TXTS_FOLDER):
#     if f.endswith('.txt'):
#         with open(f'{TXTS_FOLDER}/{f}') as txt:
#             lines = txt.readlines()
#             first_lines.append(lines[0])
# with open('f_lines.txt', 'w') as lines:
#     lines.writelines(first_lines)
# exit(0)

poems, lines, pass_counts, poetry_ratio = poetrify_file(None, f'{TXTS_FOLDER}/{ebook_id}.txt')
print(f'caught {len(lines)} lines of poetry ({poetry_ratio})')
print(pass_counts)
if poems is None:
    print('no poems')

with open(f'{JSON_FOLDER}/{ebook_id}-poetri_test.json', 'w') as json_file:
    json.dump(poems, json_file, indent=2)

if len(lines) > 0:
    with open(f'{JSON_FOLDER}/{ebook_id}-poetri.ndjson', 'w') as ndjson_file:
        ndjson_file.write('\n'.join([json.dumps(line) for line in lines]))
