## YAGA 0.7
_**Y**et **A**nother **G**utenberg **A**ggregator_ is a terminal user interface for helping digital librarians and robo-poets getting raw text and nlp right of the [Gutenberg](https://www.gutenberg.org/) catalog.

### usage
```
python -m yaga
```
There is 4 major commands:
- `pull` chains the logic of keeping the local data sync with the actual catalog hosted on Gutenberg; meaning both the unarchived rdf's and records from a `sqlite` database;
- `clear` obviously takes care of clearing that local cache;
- `find` lets you query the database to find records of books matching filters such as subjects, authors and language;
- `explore` lets you access your local bookshelf of plain text files and automates nlp tasks on them (_in english only_).

 ### devnotes
 #### 06/02/2020
 Jesus, it's 2020, nobody cares about this shit but I'm upgrading the code to prompt_toolkit > 3.0 etc… Yeah whatever fucking overcrowded Internet.
 #### 05/03/19
 Syncing the catalog takes a very long horsy dickin time. Reasons are: usage of sqlite, producer/consumer mutlithreading commuication through the queue needs to sleep some time for the UI to refresh logs. I use sqlite because of sql, so to speed up that part, I can only optmize the _where_ and _when_ I pass queries to the database. At least, we should batch insert by packet of 50/100 books and not query the database everytime like we're doing now. Also, the `stats` table may also be usefull. For the logging part, I could log less and also compute the smallest time possible for the sleep UI refresh ("given what?").
#### 20/03/19
Keeping a list of thoroughly tested books while improving poetrification:
- `pg8699` - **Dante**. _The vision of Hell, Purgatory, and Paradise_;
- `pg8905` - **Wordsworth**. _Lyrical Ballads, With Other Poems_;

#### 31/03/19
Checks that we could add:
- when a stanza is mostly made of white space before the text -> it's a centered title. For instance in `pg20185`:

                            WIMBLEDON;

                                OR

                    THE HERMIT OF THE CEDARS.




                            CHAPTER I.

        "The stars are out, and by their glistening light,
        I fain would whisper in thine ear a tale;
        Wilt hear it kindly? and if long and dull
        Believe me far more deeply grieved than thou."
- when we have a oneliner close to a stanza, we should find out if it's snappable and introduce a `gap` in that range. This mostly occurs in plays, eg in `pg8905`:

        VI.

        FIRST VOICE.

        "But tell me, tell me! speak again,
            Thy soft response renewing--
        What makes that ship drive on so fast?
            What is the Ocean doing?"

        SECOND VOICE.

        "Still as a Slave before his Lord,
            The Ocean hath no blast:
        His great bright eye most silently
            Up to the moon is cast--"

        "If he may know which way to go,
            For she guides him smooth or grim,
        See, brother, see! how graciously
            She looketh down on him."

        FIRST VOICE.

        "But why drives on that ship so fast
            Without or wave or wind?"

        SECOND VOICE.

        "The air is cut away before,
            And closes from behind."

### dependencies
- [prompt_toolkit](https://github.com/prompt-toolkit/python-prompt-toolkit)

### TODO's
- [ ] add a `deeper` checkbox to the _poetrify_ tool of the explorer to compute more thorough study of poems (there might be a way there to expel remaining garbage);
- [ ] _@v1_ add `from` and `until` keyword validator for the query (based on author birth and death dates);
- [ ] _@v1_ compute a _Remaing time..._ for long lasting operations;
- [ ] _@v1.1_ optimize the overall speed of the `pull` command;
- [ ] _@v1.2_ clean all those `assert` so they only make sense at execution time...

### HISTORY
#### 0.2
- fix logs glitches during the `pull` taks' progression;
- add a language filter in the query dialog.

#### 0.3
- implement the `explore` command;
- add a `urls` table to store the Gutenberg ebooks' links for every major formats;
- implements dowloading capability for utf-8 plain text.

#### 0.4
- indicates the presence of a local copy in the explorer;
- implements `s-down`-`s-up` keybindings to hide/reveal the console.

#### 0.5
- implements a debug version of the `clear` command;
- implements raw poetry lines extraction and a draft-yet-ok poems aggregator.

#### 0.6
- more robust poetry parser with a _hole puncher_ marking gaps of none poetry lines in ranges of text (blocks) that we think to be belonging to the same poem.
- make poetrifier exports to [ndjson](http://ndjson.org/) for saving lines of poetry or a compact list of ranges & holes only; `json` would contain a plain copy of poetry content as a list of poems.

#### 0.7
- (a) numerous bug fixes, faster SQL insertions.
- (b) no changes but dependencies, bitch.